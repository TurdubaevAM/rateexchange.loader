﻿using RateExchange.Common.Utility.Parser;
using RateExchange.Service.DataTypes;
using RateExchange.Service.DataTypes.Parser;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RateExchange.Service.Abstraction {
    public interface IRateExchangeService {
        Task AddDailyRateExchange(IDocument<DailyRateExchangeDocumentMetaData, DailyRateExchangeDocumentHeader, DailyRateExchangeDocumentRow> document);
        Task AddHistoryRangeExchange(IDocument<DailyRateExchangeDocumentHeader, DailyRateExchangeDocumentRow> document);
        Task<MonthRateExchangeReportDTO> GetRateExchangeReport(DateTime beginDate, DateTime endDate, IEnumerable<int> currencies);
    }
}
