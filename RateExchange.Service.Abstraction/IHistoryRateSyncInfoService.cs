﻿using RateExchange.Service.DataTypes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RateExchange.Service.Abstraction {
    public interface IHistoryRateSyncInfoService {
        Task<List<HistoryRateSyncInfoDTO>> GetSyncedHistoryRates();
        Task SetSyncInfo(int syncedYear);
    }
}
