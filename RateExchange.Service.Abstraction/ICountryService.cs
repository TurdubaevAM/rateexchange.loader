﻿using RateExchange.Service.DataTypes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RateExchange.Service.Abstraction {
    public interface ICountryService {
        Task<List<CountryDTO>> GetCountries();
    }
}
