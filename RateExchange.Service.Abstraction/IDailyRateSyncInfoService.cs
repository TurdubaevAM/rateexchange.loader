﻿using RateExchange.Service.DataTypes;
using System.Threading.Tasks;

namespace RateExchange.Service.Abstraction {
    public interface IDailyRateSyncInfoService {
        Task<DailyRateSyncInfoDTO> GetLastSyncDate();
        Task SetLastSyncDate();
    }
}
