﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations
{
    public partial class CreateCountryAndCurrencyTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "DailyRateExchange");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "DailyRateExchange");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "DailyRateExchange");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "DailyRateExchange",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "DailyRateExchange",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DailyRateExchange_CountryId",
                table: "DailyRateExchange",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_DailyRateExchange_CurrencyId",
                table: "DailyRateExchange",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_DailyRateExchange_Country_CountryId",
                table: "DailyRateExchange",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DailyRateExchange_Currency_CurrencyId",
                table: "DailyRateExchange",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DailyRateExchange_Country_CountryId",
                table: "DailyRateExchange");

            migrationBuilder.DropForeignKey(
                name: "FK_DailyRateExchange_Currency_CurrencyId",
                table: "DailyRateExchange");

            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropIndex(
                name: "IX_DailyRateExchange_CountryId",
                table: "DailyRateExchange");

            migrationBuilder.DropIndex(
                name: "IX_DailyRateExchange_CurrencyId",
                table: "DailyRateExchange");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "DailyRateExchange");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "DailyRateExchange");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "DailyRateExchange",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "DailyRateExchange",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "DailyRateExchange",
                nullable: true);
        }
    }
}
