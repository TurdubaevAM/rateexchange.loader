﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations {
    public partial class FillInCountryTable : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.Sql(@"
INSERT INTO Country VALUES ('Australia', 'AUD'  )
INSERT INTO Country VALUES ('Brazil', 'BRL' )
INSERT INTO Country VALUES ('Bulgaria', 'BGN'   )
INSERT INTO Country VALUES ('Canada', 'CAD' )
INSERT INTO Country VALUES ('China', 'CNY'  )
INSERT INTO Country VALUES ('Croatia', 'HRK'    )
INSERT INTO Country VALUES ('Denmark', 'DKK'    )
INSERT INTO Country VALUES ('EMU', 'EUR'    )
INSERT INTO Country VALUES ('Hongkong', 'HKD'   )
INSERT INTO Country VALUES ('Hungary', 'HUF'    )
INSERT INTO Country VALUES ('Iceland', 'ISK'    )
INSERT INTO Country VALUES ('IMF', 'XDR'    )
INSERT INTO Country VALUES ('India', 'INR'  )
INSERT INTO Country VALUES ('Indonesia', 'IDR'  )
INSERT INTO Country VALUES ('Israel', 'ILS' )
INSERT INTO Country VALUES ('Japan', 'JPY'  )
INSERT INTO Country VALUES ('Malaysia', 'MYR'   )
INSERT INTO Country VALUES ('Mexico', 'MXN' )
INSERT INTO Country VALUES ('New Zealand', 'NZD'    )
INSERT INTO Country VALUES ('Norway', 'NOK' )
INSERT INTO Country VALUES ('Philippines', 'PHP'    )
INSERT INTO Country VALUES ('Poland', 'PLN' )
INSERT INTO Country VALUES ('Romania new', 'RON'    )
INSERT INTO Country VALUES ('Russia', 'RUB' )
INSERT INTO Country VALUES ('Singapore', 'SGD'  )
INSERT INTO Country VALUES ('South Africa', 'ZAR'   )
INSERT INTO Country VALUES ('South Korea', 'KRW'    )
INSERT INTO Country VALUES ('Sweden', 'SEK' )
INSERT INTO Country VALUES ('Switzerland', 'CHF'    )
INSERT INTO Country VALUES ('Thailand', 'THB'   )
INSERT INTO Country VALUES ('Turkey', 'TRY' )
INSERT INTO Country VALUES ('United Kingdom', 'GBP' )
INSERT INTO Country VALUES ('USA', 'USD'    )
");
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.Sql(@"
DELETE FROM [Country];
DBCC CHECKIDENT ('[Country]', RESEED, 0);
");
        }
    }
}
