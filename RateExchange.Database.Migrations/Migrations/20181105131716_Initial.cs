﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DailyRateInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyRateInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DailyRateExchange",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Rate = table.Column<decimal>(nullable: false),
                    DailyRateInfoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyRateExchange", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DailyRateExchange_DailyRateInfo_DailyRateInfoId",
                        column: x => x.DailyRateInfoId,
                        principalTable: "DailyRateInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DailyRateExchange_DailyRateInfoId",
                table: "DailyRateExchange",
                column: "DailyRateInfoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DailyRateExchange");

            migrationBuilder.DropTable(
                name: "DailyRateInfo");
        }
    }
}
