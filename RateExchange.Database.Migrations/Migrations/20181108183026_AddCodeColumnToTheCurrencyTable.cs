﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations
{
    public partial class AddCodeColumnToTheCurrencyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Country");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Currency",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Currency");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Country",
                nullable: true);
        }
    }
}
