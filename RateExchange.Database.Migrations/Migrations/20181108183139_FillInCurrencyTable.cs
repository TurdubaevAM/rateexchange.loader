﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations {
    public partial class FillInCurrencyTable : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.Sql(@"
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar', 1 , 'AUD')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('real',  2, 'BRL')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('lev',  3, 'BGN')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar',  4, 'CAD')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('renminbi', 5 , 'CNY')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('kuna',  6, 'HRK')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('krone',  7, 'DKK')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('euro',  8, 'EUR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar', 9 , 'HKD')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('forint', 10 , 'HUF')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('krona', 11 , 'ISK')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('SDR', 12 , 'XDR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('rupee', 13 , 'INR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('rupiah', 14 , 'IDR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('shekel', 15 , 'ILS')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('yen', 16 , 'JPY')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('ringgit', 17 , 'MYR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('peso', 18 , 'MXN')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar', 19 , 'NZD')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('krone', 20 , 'NOK')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('peso', 21 , 'PHP')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('zloty', 22 , 'PLN')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('new leu', 23 , 'RON')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('rouble', 24 , 'RUB')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar', 25 , 'SGD')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('rand', 26 , 'ZAR')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('won', 27 , 'KRW')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('krona', 28 , 'SEK')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('franc', 29 , 'CHF')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('baht', 30 , 'THB')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('lira', 31 , 'TRY')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('pound', 32 , 'GBP')
INSERT INTO Currency (Name, CountryId, Code) VALUES ('dollar', 33 , 'USD')
");
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.Sql(@"
DELETE FROM [Currency];
DBCC CHECKIDENT ('[Currency]', RESEED, 0);
");
        }
    }
}
