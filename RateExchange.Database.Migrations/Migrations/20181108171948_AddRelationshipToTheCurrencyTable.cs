﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RateExchange.Database.Migrations.Migrations
{
    public partial class AddRelationshipToTheCurrencyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Currency",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Currency_CountryId",
                table: "Currency",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Currency_Country_CountryId",
                table: "Currency",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Currency_Country_CountryId",
                table: "Currency");

            migrationBuilder.DropIndex(
                name: "IX_Currency_CountryId",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Currency");
        }
    }
}
