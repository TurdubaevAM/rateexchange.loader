﻿using Microsoft.AspNetCore.Mvc;
using RateExchange.API.Models;
using RateExchange.API.Models.RateExchange;
using RateExchange.Common.Enum;
using RateExchange.Common.Exceptions;
using RateExchange.Service.Abstraction;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.API.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class RateExchangeController : ControllerBase {
        private readonly IRateExchangeService _rateExchangeService;
        private readonly ICurrencyService _currencyService;


        public RateExchangeController(IRateExchangeService rateExchangeService, ICurrencyService currencyService) {
            _rateExchangeService = rateExchangeService ??
                throw new ArgumentNullException(nameof(rateExchangeService));

            _currencyService = currencyService ??
                throw new ArgumentNullException(nameof(currencyService));
        }

        [HttpGet("report")]
        public async Task<ActionResult> GetReport([FromQuery(Name = "currencies")] int[] currencies, int month = 1, int year = 2018, int fileType = 1) {

            DateTime beginDate = new DateTime(year, month, 1);
            DateTime endDate = beginDate.AddMonths(1);

            var rates = await _rateExchangeService.GetRateExchangeReport(beginDate, endDate, currencies);
            var report = new MonthReport() {
                Month = rates.Month,
                Year = rates.Year,
                Weeks = rates.WeekPeriods.Select(x => new WeekReport() {
                    StartDay = x.WeekStartDayNumber,
                    EndDay = x.WeekEndDayNumber,
                    Rates = x.RatesExchange.Select(r => new RateExchangeInfo() {
                        Min = r.Min,
                        Max = r.Max,
                        Mediana = r.Mediana,
                        Currency = r.Currency
                    }).ToList()
                }).ToList()
            };

            var type = (FileType)fileType;

            switch (type) {
                case FileType.Text:
                    var fileName = $"Report { beginDate.ToString("yyyy-MM-dd")} - {endDate.ToString("yyyy-MM-dd")}.txt";
                    return File(await report.ExportAsync(type), "application/octet-stream", fileName);

                case FileType.Json:
                default:
                    return new ApiJsonResult(report);
            }
        }

        [HttpGet("currency")]
        public async Task<JsonResult> GetCurrencies() {
            var currencies = await _currencyService.GetCurrencies();

            return new ApiJsonResult(currencies);
        }
    }
}
