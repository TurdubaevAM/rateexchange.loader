﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RateExchange.API.Extension;
using RateExchange.API.Middleware;
using RateExchange.API.Settings;
using RateExchange.Database;

namespace RateExchange.API {

    public class Startup {
        public IConfiguration Configuration { get; }
        public static SecretSettings SecretSettings { get; set; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            SecretSettings = Configuration
                .GetSection("SecretSettings")
                .Get<SecretSettings>();

            var connectionString = Configuration.GetConnectionString("RateExchangeDB");
            services.AddDbContext<SqlServerDbContext>(options =>
                options.UseSqlServer(connectionString, b => b.MigrationsAssembly("RateExchange.Database.Migrations")));

            services.RegisterDI();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMiddleware(typeof(ExceptionHandlingMiddleware));
            app.UseMvc();
        }
    }
}
