﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RateExchange.API.Models.RateExchange {
    internal sealed class WeekReport {

        [JsonProperty(PropertyName = "StartDay")]
        public int StartDay { get; set; }

        [JsonProperty(PropertyName = "EndDay")]
        public int EndDay { get; set; }

        [JsonProperty(PropertyName = "Rates")]
        public List<RateExchangeInfo> Rates { get; set; }
    }
}
