﻿using Newtonsoft.Json;

namespace RateExchange.API.Models.RateExchange {
    internal sealed class RateExchangeInfo {
        [JsonProperty(PropertyName = "Currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "Min")]
        public decimal Min { get; set; }

        [JsonProperty(PropertyName = "Max")]
        public decimal Max { get; set; }

        [JsonProperty(PropertyName = "Mediana")]
        public decimal Mediana { get; set; }
    }
}
