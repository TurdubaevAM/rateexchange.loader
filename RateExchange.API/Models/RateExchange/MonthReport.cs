﻿using Newtonsoft.Json;
using RateExchange.Common.Enum;
using RateExchange.Service.DataTypes.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateExchange.API.Models.RateExchange {
    internal sealed class MonthReport : IExported {
        [JsonProperty(PropertyName = "Year")]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "Month")]
        public string Month { get; set; }

        [JsonProperty(PropertyName = "WeekPeriods")]
        public List<WeekReport> Weeks { get; set; }

        public async Task<byte[]> ExportAsync(FileType type) {
            using (var memoryStream = new MemoryStream()) {
                using (var writer = new StreamWriter(memoryStream)) {
                    var reportTitle = $"Year: { Year }, month: { Month }";
                    await writer.WriteLineAsync(reportTitle);

                    if (Weeks == null || !Weeks.Any())
                        return await Task.FromResult(memoryStream.ToArray());

                    await writer.WriteLineAsync("Week periods:");
                    foreach (var week in Weeks) {
                        StringBuilder sb = new StringBuilder($"{week.StartDay}...{week.EndDay}: ");
                        foreach (var rate in week.Rates) {
                            sb.Append($"{rate.Currency} - max: {rate.Max}, min: {rate.Min}, median: {rate.Mediana}; ");
                        }
                        sb.Append(Environment.NewLine);
                        await writer.WriteAsync(sb.ToString());
                    }

                    await writer.FlushAsync();
                    memoryStream.Position = 0;

                    return await Task.FromResult(memoryStream.ToArray());
                }
            }
        }
    }
}
