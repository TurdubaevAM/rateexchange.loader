﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace RateExchange.API.Models {
    public class ApiJsonResult : JsonResult {

        public ApiJsonResult(object data) :
            base(data, new JsonSerializerSettings() {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Formatting = Formatting.Indented
            }) {
        }
    }
}
