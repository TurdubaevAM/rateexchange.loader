﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RateExchange.Common.Exceptions;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RateExchange.API.Middleware {
    public class ExceptionHandlingMiddleware {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next) {
            this.next = next;
        }

        public async Task Invoke(HttpContext context) {
            try {
                await next(context);
            }
            catch (Exception ex) {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception) {
            var code = exception is ExceptionBase
                ? HttpStatusCode.BadRequest
                : HttpStatusCode.InternalServerError;

            var response = JsonConvert.SerializeObject(new { error = exception.Message });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(response);
        }
    }
}
