﻿using Microsoft.Extensions.DependencyInjection;
using RateExchange.Database;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service;
using RateExchange.Service.Abstraction;

namespace RateExchange.API.Extension {
    internal static class DIExtension {
        public static void RegisterDI(this IServiceCollection serviceCollection) {
            serviceCollection.RegisterServices();
            serviceCollection.RegisterRepositories();
            serviceCollection.RegisterDatabaseProviders();
        }

        private static void RegisterServices(this IServiceCollection serviceCollection) {
            serviceCollection.AddScoped<IRateExchangeService, RateExchangeService>();
            serviceCollection.AddScoped<ICurrencyService, CurrencyService>();
            serviceCollection.AddScoped<ICountryService, CountryService>();
            serviceCollection.AddScoped<IHistoryRateSyncInfoService, HistoryRateSyncInfoService>();
            serviceCollection.AddScoped<IDailyRateSyncInfoService, DailyRateSyncInfoService>();
        }

        private static void RegisterRepositories(this IServiceCollection serviceCollection) {
            serviceCollection.AddScoped<IRateExchangeRepository, RateExchangeRepository>();
            serviceCollection.AddScoped<IRepository<Country>, BaseRepository<Country>>();
            serviceCollection.AddScoped<IRepository<Currency>, BaseRepository<Currency>>();
            serviceCollection.AddScoped<IRepository<HistoryRatesSyncInfo>, BaseRepository<HistoryRatesSyncInfo>>();
            serviceCollection.AddScoped<IRepository<DailyRateSyncInfo>, BaseRepository<DailyRateSyncInfo>>();
        }

        private static void RegisterDatabaseProviders(this IServiceCollection serviceCollection) {
            serviceCollection.AddScoped<DbContextBase, SqlServerDbContext>();
        }
    }
}
