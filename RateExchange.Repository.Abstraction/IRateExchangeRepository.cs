﻿using RateExchange.Database.Entity;
using RateExchange.Service.DataTypes;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RateExchange.Database.Repository.Abstraction {
    public interface IRateExchangeRepository : IRepository<DailyRateInfo> {
        Task AddDailyRateExchangeAsync(DailyRateInfo dailyRateInfo);
        Task AddDailyRateExchangeAsync(List<DailyRateInfo> dailyRatesInfo);
        Task<MonthRateExchangeReportDTO> GetRateExchangeReport(DateTime beginDate, DateTime endDate, IEnumerable<int> currencies);
    }
}
