﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Database.Repository.Abstraction {
    public interface IRepository<T, TK> : IDisposable
         where T : class
         where TK : struct {

        T GetById(TK id);
        Task<T> GetByIdAsync(TK id);
        IEnumerable<T> GetAll();
        IQueryable<T> AsQueryable();

        T Create(T newEntity);
        void Update(T entity);

        T Delete(TK id);
        void DeleteMany(List<TK> ids);

        void Commit();
        Task<int> CommitAsync();
    }

    public interface IRepository<T> : IRepository<T, int>
        where T : class {
    }
}
