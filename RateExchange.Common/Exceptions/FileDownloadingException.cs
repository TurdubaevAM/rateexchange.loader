﻿using System;

namespace RateExchange.Common.Exceptions {
    public class FileDownloadingException : ExceptionBase {
        public FileDownloadingException() : base() {
        }

        public FileDownloadingException(string message) : base(message) {
        }

        public FileDownloadingException(string message, Exception ex) : base(message, ex) {
        }
    }
}
