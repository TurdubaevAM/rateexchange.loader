﻿using System;

namespace RateExchange.Common.Exceptions {
    public class EntityNotFoundException : ExceptionBase {
        public EntityNotFoundException() : base() {
        }

        public EntityNotFoundException(string message) : base(message) {
        }

        public EntityNotFoundException(string message, Exception ex) : base(message, ex) {
        }
    }
}