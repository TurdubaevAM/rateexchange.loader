﻿using System;

namespace RateExchange.Common.Exceptions {
    public class ExceptionBase : Exception {

        public ExceptionBase() : base() {
        }

        public ExceptionBase(string message) : base(message) {
        }

        public ExceptionBase(string message, Exception ex) : base(message, ex) {
        }
    }
}
