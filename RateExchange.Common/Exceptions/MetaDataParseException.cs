﻿using System;

namespace RateExchange.Common.Exceptions {
    public class MetaDataParseException : ExceptionBase {
        public MetaDataParseException() : base() {
        }

        public MetaDataParseException(string message) : base(message) {
        }

        public MetaDataParseException(string message, Exception ex) : base(message, ex) {
        }
    }
}
