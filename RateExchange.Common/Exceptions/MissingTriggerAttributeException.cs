﻿using System;

namespace RateExchange.Common.Exceptions {
    public class MissingTriggerAttributeException : ExceptionBase {
        public MissingTriggerAttributeException() : base() {
        }

        public MissingTriggerAttributeException(string message) : base(message) {
        }

        public MissingTriggerAttributeException(string message, Exception ex) : base(message, ex) {
        }
    }
}
