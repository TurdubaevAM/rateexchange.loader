﻿using System.Configuration;
using System.Reflection;

namespace RateExchange.Common.Settings {
    public static class AppSettings {

        public static string ConnectionString {
            get {
                var appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

                string connectionString = appConfig.AppSettings.CurrentConfiguration.ConnectionStrings.ConnectionStrings["RateExchangeDB"].ConnectionString;
                return connectionString;
            }
        }
    }
}
