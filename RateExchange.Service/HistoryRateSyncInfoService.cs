﻿using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Service {
    public class HistoryRateSyncInfoService : IHistoryRateSyncInfoService {
        private readonly IRepository<HistoryRatesSyncInfo> _historySyncInfoRepository;

        public HistoryRateSyncInfoService(IRepository<HistoryRatesSyncInfo> historySyncInfoRepository) {
            _historySyncInfoRepository = historySyncInfoRepository ??
                throw new ArgumentNullException(nameof(historySyncInfoRepository));
        }

        public async Task<List<HistoryRateSyncInfoDTO>> GetSyncedHistoryRates() {
            var dbSyncInfo = _historySyncInfoRepository.GetAll();
            var syncInfo = dbSyncInfo.Select(x => new HistoryRateSyncInfoDTO() {
                Id = x.Id,
                LastSyncDate = x.LastSyncDate,
                SyncYear = x.SyncYear
            }).ToList();

            return await Task.FromResult(syncInfo);
        }

        public async Task SetSyncInfo(int syncedYear) {
            var dbSyncInfo = _historySyncInfoRepository.AsQueryable().FirstOrDefault(x => x.SyncYear == syncedYear);
            if (dbSyncInfo == null) {
                dbSyncInfo = new HistoryRatesSyncInfo() {
                    LastSyncDate = DateTime.UtcNow,
                    SyncYear = syncedYear
                };
                _historySyncInfoRepository.Create(dbSyncInfo);
            }
            else {
                dbSyncInfo.LastSyncDate = DateTime.UtcNow;
            }

            await _historySyncInfoRepository.CommitAsync();

        }
    }
}
