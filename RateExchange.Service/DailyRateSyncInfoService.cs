﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes;
using System;
using System.Threading.Tasks;

namespace RateExchange.Service {
    public class DailyRateSyncInfoService : IDailyRateSyncInfoService {
        private readonly IRepository<DailyRateSyncInfo> _dailyRateSyncInfoRepository;

        public DailyRateSyncInfoService(IRepository<DailyRateSyncInfo> dailyRateSyncInfoRepository) {
            _dailyRateSyncInfoRepository = dailyRateSyncInfoRepository ??
                throw new ArgumentNullException(nameof(dailyRateSyncInfoRepository));
        }

        public async Task<DailyRateSyncInfoDTO> GetLastSyncDate() {
            var dbLastSyncInfo = await _dailyRateSyncInfoRepository.AsQueryable().LastOrDefaultAsync();
            if (dbLastSyncInfo == null)
                return null;

            var syncInfo = new DailyRateSyncInfoDTO() {
                Id = dbLastSyncInfo.Id,
                LastSyncDate = dbLastSyncInfo.LastSyncDate
            };

            return syncInfo;
        }

        public async Task SetLastSyncDate() {
            var dbLastSyncInfo = await _dailyRateSyncInfoRepository.AsQueryable().LastOrDefaultAsync();
            if (dbLastSyncInfo == null) {
                dbLastSyncInfo = new DailyRateSyncInfo() { LastSyncDate = DateTime.UtcNow };
                _dailyRateSyncInfoRepository.Create(dbLastSyncInfo);
            }
            else
                dbLastSyncInfo.LastSyncDate = DateTime.UtcNow;

            await _dailyRateSyncInfoRepository.CommitAsync();
        }
    }
}
