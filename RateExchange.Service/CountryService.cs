﻿using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Service {
    public class CountryService : ICountryService {
        private readonly IRepository<Country> _countryRepository;

        public CountryService(IRepository<Country> countryRepository) {
            _countryRepository = countryRepository ??
                throw new ArgumentNullException(nameof(countryRepository));
        }

        public async Task<List<CountryDTO>> GetCountries() {
            var dbCountries = _countryRepository.GetAll();
            var countries = dbCountries.Select(x => new CountryDTO() {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return await Task.FromResult(countries);
        }
    }
}
