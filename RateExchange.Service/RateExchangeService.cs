﻿using RateExchange.Common.Exceptions;
using RateExchange.Common.Utility.Parser;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes;
using RateExchange.Service.DataTypes.Parser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Service {
    public class RateExchangeService : IRateExchangeService {
        private readonly IRateExchangeRepository _rateExchangeRepository;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;

        public RateExchangeService(IRateExchangeRepository rateExchangeRepository, ICountryService countryService, ICurrencyService currencyService) {
            _rateExchangeRepository = rateExchangeRepository ??
                throw new ArgumentNullException(nameof(rateExchangeRepository));

            _countryService = countryService ??
                throw new ArgumentNullException(nameof(countryService));

            _currencyService = currencyService ??
                throw new ArgumentNullException(nameof(currencyService));
        }

        public async Task AddDailyRateExchange(IDocument<DailyRateExchangeDocumentMetaData, DailyRateExchangeDocumentHeader, DailyRateExchangeDocumentRow> document) {
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            if (String.IsNullOrWhiteSpace(document.MetaData?.Description))
                throw new MetaDataParseException("Unable to recognize the meta data description for the daily rate exchange document");

            var metaDataArray = document.MetaData.Description.Split('#', StringSplitOptions.RemoveEmptyEntries);
            var dailyRate = new DailyRateInfo() {
                Number = int.Parse(metaDataArray[1]),
                Date = DateTime.ParseExact(metaDataArray[0], "dd.MMM yyyy ", CultureInfo.InvariantCulture)
            };

            var currencies = await _currencyService.GetCurrencies();

            dailyRate.Rates = new List<DailyRateExchange>();
            foreach (var record in document.Records) {
                var currencyCode = record.Columns[3];
                var currencyName = record.Columns[1];

                var currency = currencies.SingleOrDefault(x =>
                    String.Equals(currencyCode, x.Code, StringComparison.InvariantCultureIgnoreCase) &&
                    String.Equals(currencyName, x.Name, StringComparison.InvariantCultureIgnoreCase));

                if (currency != null) {
                    var rate = new DailyRateExchange() {
                        CountryId = currency.CountryId,
                        Amount = int.Parse(record.Columns[2]),
                        Rate = decimal.Parse(record.Columns[4], CultureInfo.InvariantCulture),
                        CurrencyId = currency.Id
                    };

                    dailyRate.Rates.Add(rate);
                }
                else
                    throw new EntityNotFoundException($"Could not find the currency record using the {currencyCode} code");
            }

            await _rateExchangeRepository.AddDailyRateExchangeAsync(dailyRate);
        }

        public async Task AddHistoryRangeExchange(
            IDocument<DailyRateExchangeDocumentHeader, DailyRateExchangeDocumentRow> document) {
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            var currencies = await _currencyService.GetCurrencies();

            const int dateTimeColumnIndex = 0;
            var dailyRatesInfo = new List<DailyRateInfo>();

            foreach (var record in document.Records) {
                var dailyRateInfo = new DailyRateInfo();
                int columnIndex = 0;

                foreach (var column in record.Columns) {

                    if (columnIndex == dateTimeColumnIndex) {
                        dailyRateInfo.Date = DateTime.ParseExact(column, "dd.MMM yyyy", CultureInfo.InvariantCulture);
                        dailyRateInfo.Rates = new List<DailyRateExchange>();
                        columnIndex++;
                        continue;
                    }

                    var splittedRate = document.Header.Columns[columnIndex].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    var currency = currencies.SingleOrDefault(x =>
                        String.Equals(x.Code, splittedRate[1], StringComparison.InvariantCultureIgnoreCase));

                    if (currency != null) {
                        var dailyRate = new DailyRateExchange() {
                            Amount = int.Parse(splittedRate[0]),
                            CountryId = currency.CountryId,
                            CurrencyId = currency.Id,
                            Rate = Decimal.Parse(record.Columns[columnIndex], CultureInfo.InvariantCulture),
                        };
                        dailyRateInfo.Rates.Add(dailyRate);

                        columnIndex++;
                    }
                    else
                        throw new EntityNotFoundException($"Could not find the currency record using the {splittedRate[1]} code");

                }
                dailyRatesInfo.Add(dailyRateInfo);
            }

            await _rateExchangeRepository.AddDailyRateExchangeAsync(dailyRatesInfo);
        }

        public async Task<MonthRateExchangeReportDTO> GetRateExchangeReport(DateTime beginDate, DateTime endDate, IEnumerable<int> currencies) {
            return await _rateExchangeRepository.GetRateExchangeReport(beginDate, endDate, currencies);
        }
    }
}
