﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Service {
    public class CurrencyService : ICurrencyService {
        private readonly IRepository<Currency> _currencyRepository;

        public CurrencyService(IRepository<Currency> currencyRepository) {
            _currencyRepository = currencyRepository
                ?? throw new ArgumentNullException(nameof(currencyRepository));
        }

        public async Task<List<CurrencyDTO>> GetCurrencies() {
            var dbCurrencies = await _currencyRepository.AsQueryable().Include(x => x.Country).ToListAsync();
            var currencies = dbCurrencies.Select(x => new CurrencyDTO() {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                CountryId = x.CountryId,
                CountryName = x.Country.Name
            }).ToList();

            return currencies;
        }
    }
}
