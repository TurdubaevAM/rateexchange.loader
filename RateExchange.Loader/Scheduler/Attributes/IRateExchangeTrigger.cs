﻿namespace RateExchange.Loader.Scheduler.Attributes {
    internal interface IRateExchangeTrigger {
        string Name { get; }
    }
}
