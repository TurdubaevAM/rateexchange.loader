﻿using System;

namespace RateExchange.Loader.Scheduler.Attributes {
    internal class SimpleTriggerAttribute : Attribute, IRateExchangeTrigger {
        public string Name { get; private set; }

        public SimpleTriggerAttribute(string attributeName) {
            Name = String.IsNullOrWhiteSpace(attributeName)
                ? throw new ArgumentNullException(nameof(attributeName))
                : attributeName;
        }
    }
}
