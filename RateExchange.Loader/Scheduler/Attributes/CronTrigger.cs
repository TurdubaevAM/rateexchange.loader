﻿using System;

namespace RateExchange.Loader.Scheduler.Attributes {
    internal class CronTrigger : Attribute, IRateExchangeTrigger {
        public string Name { get; private set; }

        public CronTrigger(string attributeName) {
            Name = String.IsNullOrWhiteSpace(attributeName)
                ? throw new ArgumentNullException(nameof(attributeName))
                : attributeName;
        }
    }
}
