﻿using System.Configuration;

namespace RateExchange.Loader.Scheduler {
    internal static class SchedulerSettings {

        public static string DailyRateDownloadingCronExpression =>
            ConfigurationManager.AppSettings.Get("DailyRateDownloadingCronExpression");

        public static int HistoryRateDownloadingTimeInterval =>
            int.Parse(ConfigurationManager.AppSettings.Get("HistoryRateDownloadingTimeInterval"));

        public static string DailyRateExchangeUrl =>
            ConfigurationManager.AppSettings.Get("DailyRateExchangeUrl");

        public static string HistoryRateExchangeUrl =>
            ConfigurationManager.AppSettings.Get("HistoryRateExchangeUrl");
    }
}
