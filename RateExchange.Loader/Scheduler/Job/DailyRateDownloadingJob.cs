﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using RateExchange.Common.Logger;
using RateExchange.Common.Utility.Helper;
using RateExchange.Common.Utility.Parser;
using RateExchange.Loader.Scheduler.Attributes;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes.Parser;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RateExchange.Loader.Scheduler.Job {

    [CronTrigger("DailyRateDownloadingTrigger")]
    internal class DailyRateDownloadingJob : IJob {
        private IServiceProvider _serviceProvider = null;

        public async Task Execute(IJobExecutionContext context) {
            var logger = FileLogger.GetInstance();

            try {
                var schedulerContext = context.Scheduler.Context;
                var serviceCollection = (IServiceCollection)schedulerContext.Get("IServiceCollection");
                _serviceProvider = serviceCollection.BuildServiceProvider();

                var currentDate = DateTime.UtcNow;

                var syncInfoService = _serviceProvider.GetRequiredService<IDailyRateSyncInfoService>();
                var syncInfo = await syncInfoService.GetLastSyncDate();
                if (syncInfo != null && syncInfo.LastSyncDate.Date == currentDate.Date)
                    return;

                var filePath = $"{ Directory.GetCurrentDirectory() }\\Reports\\Daily\\{ currentDate.ToString("yyyy_MM_dd_HH_mm_ss") }.txt";
                var fileUrl = SchedulerSettings.DailyRateExchangeUrl + currentDate.ToString("dd.MM.yyyy");

                await HttpHelper.DownloadFileAsync(fileUrl, filePath);

                var parser = new TextFileWithMetadataParser<
                    DailyRateExchangeDocument,
                    DailyRateExchangeDocumentMetaData,
                    DailyRateExchangeDocumentHeader,
                    DailyRateExchangeDocumentRow>();
                var rateExchange = await parser.LoadAsync(filePath);

                var rateExchangeService = _serviceProvider.GetRequiredService<IRateExchangeService>();
                await rateExchangeService.AddDailyRateExchange(rateExchange);
                await syncInfoService.SetLastSyncDate();
            }
            catch (Exception ex) {
                await logger.LogAsync(ex);
            }
        }
    }
}
