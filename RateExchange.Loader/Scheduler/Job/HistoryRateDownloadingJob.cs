﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using RateExchange.Common.Logger;
using RateExchange.Common.Utility.Helper;
using RateExchange.Common.Utility.Parser;
using RateExchange.Loader.Scheduler.Attributes;
using RateExchange.Service.Abstraction;
using RateExchange.Service.DataTypes.Parser;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Loader.Scheduler.Job {

    [CronTrigger("HistoryRateDownloadingTrigger")]
    internal class HistoryRateDownloadingJob : IJob {
        private IServiceProvider _serviceProvider = null;

        public async Task Execute(IJobExecutionContext context) {
            var logger = FileLogger.GetInstance();

            try {
                var schedulerContext = context.Scheduler.Context;
                var serviceCollection = (IServiceCollection)schedulerContext.Get("IServiceCollection");
                _serviceProvider = serviceCollection.BuildServiceProvider();

                var historyRateSyncInfoService = _serviceProvider.GetRequiredService<IHistoryRateSyncInfoService>();
                var syncedYears = await historyRateSyncInfoService.GetSyncedHistoryRates();

                int startYear = 2017;
                while (startYear <= DateTime.UtcNow.Year) {
                    if (syncedYears.Any(x => x.SyncYear == startYear))
                        continue;

                    var filePath = $"{ Directory.GetCurrentDirectory() }\\Reports\\History\\{ startYear }.txt";
                    var fileUrl = SchedulerSettings.HistoryRateExchangeUrl + startYear;

                    await HttpHelper.DownloadFileAsync(fileUrl, filePath);

                    var parser = new TextFileParser<
                        DailyRateExchangeDocument,
                        DailyRateExchangeDocumentHeader,
                        DailyRateExchangeDocumentRow>();
                    var rateExchange = await parser.LoadAsync(filePath);

                    var rateExchangeService = _serviceProvider.GetRequiredService<IRateExchangeService>();
                    await rateExchangeService.AddHistoryRangeExchange(rateExchange);

                    await historyRateSyncInfoService.SetSyncInfo(startYear);
                    startYear++;
                }
            }
            catch (Exception ex) {
                await logger.LogAsync(ex);
            }
        }
    }
}
