﻿using RateExchange.Common.Exceptions;
using RateExchange.Loader.Scheduler.Attributes;
using System;
using System.Linq;

namespace RateExchange.Loader.Extension {
    public static class TriggerExtension {
        public static string GetNameAttribute(this Type type) {
            var triggerAttr = type.GetCustomAttributes(typeof(IRateExchangeTrigger), true).FirstOrDefault() as IRateExchangeTrigger;
            if (triggerAttr != null) {
                return triggerAttr.Name;
            }

            throw new MissingTriggerAttributeException("Unable to read TriggerAttribute on the job!");
        }
    }
}
