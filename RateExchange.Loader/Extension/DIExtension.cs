﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RateExchange.Common.Settings;
using RateExchange.Database;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service;
using RateExchange.Service.Abstraction;

namespace RateExchange.Loader {
    internal static class DIExtension {
        public static void RegisterDI(this IServiceCollection serviceCollection) {
            serviceCollection.RegisterServices();
            serviceCollection.RegisterRepositories();
            serviceCollection.RegisterDatabaseProviders();
        }

        private static void RegisterServices(this IServiceCollection serviceCollection) {
            serviceCollection.AddSingleton<IRateExchangeService, RateExchangeService>();
            serviceCollection.AddSingleton<ICurrencyService, CurrencyService>();
            serviceCollection.AddSingleton<ICountryService, CountryService>();
            serviceCollection.AddSingleton<IHistoryRateSyncInfoService, HistoryRateSyncInfoService>();
            serviceCollection.AddSingleton<IDailyRateSyncInfoService, DailyRateSyncInfoService>();
        }

        private static void RegisterRepositories(this IServiceCollection serviceCollection) {
            serviceCollection.AddSingleton<IRateExchangeRepository, RateExchangeRepository>();
            serviceCollection.AddSingleton<IRepository<Country>, BaseRepository<Country>>();
            serviceCollection.AddSingleton<IRepository<Currency>, BaseRepository<Currency>>();
            serviceCollection.AddSingleton<IRepository<HistoryRatesSyncInfo>, BaseRepository<HistoryRatesSyncInfo>>();
            serviceCollection.AddSingleton<IRepository<DailyRateSyncInfo>, BaseRepository<DailyRateSyncInfo>>();
        }

        private static void RegisterDatabaseProviders(this IServiceCollection serviceCollection) {
            serviceCollection.AddDbContext<DbContextBase, SqlServerDbContext>(options =>
                options.UseSqlServer(AppSettings.ConnectionString, b => b.MigrationsAssembly("RateExchange.Database.Migrations")));
        }
    }
}