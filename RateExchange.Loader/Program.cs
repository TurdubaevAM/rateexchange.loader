﻿using Microsoft.Extensions.DependencyInjection;
using RateExchange.Common.Logger;
using System;
using System.Threading.Tasks;

namespace RateExchange.Loader {
    class Program {
        static async Task Main(string[] args) {
            var logger = FileLogger.GetInstance();

            try {
                var serviceCollection = new ServiceCollection();
                var app = new Application(serviceCollection);
                await app.RunAsync();
            }
            catch (Exception ex) {
                await logger.LogAsync(ex, LogType.ERROR);
                logger.ForceWrite();
            }
        }
    }
}
