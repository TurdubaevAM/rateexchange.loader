﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using RateExchange.Loader.Extension;
using RateExchange.Loader.Scheduler;
using RateExchange.Loader.Scheduler.Job;
using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace RateExchange.Loader {
    internal class Application {
        private const string _name = "RateExchange.Loader";
        private IScheduler _scheduler;

        public Application(IServiceCollection serviceCollection) {
            serviceCollection.RegisterDI();

            InitializeSchedulerAsync(serviceCollection)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();

        }

        public async Task RunAsync() {
            await _scheduler.Start();

            Console.WriteLine("The Rate Exchange downloader application is started to work");
            Console.WriteLine("To close the app enter :q command");

            while (!String.Equals(Console.ReadLine(), ":q", StringComparison.InvariantCultureIgnoreCase)) {

            }
        }

        private async Task InitializeSchedulerAsync(IServiceCollection serviceCollection) {
            var props = new NameValueCollection { {
                    "quartz.serializer.type", "binary"
                }
            };

            var factory = new StdSchedulerFactory(props);
            _scheduler = await factory.GetScheduler();
            _scheduler.Context.Put("IServiceCollection", serviceCollection);

            IJobDetail dailyRateDownloadingJob = JobBuilder.Create<DailyRateDownloadingJob>()
                .WithIdentity(nameof(DailyRateDownloadingJob), _name)
                .Build();

            IJobDetail historyRateDownloadingJob = JobBuilder.Create<HistoryRateDownloadingJob>()
                .WithIdentity(nameof(HistoryRateDownloadingJob), _name)
                .Build();

            var dailyRateDownloadingTrigger = TriggerBuilder.Create()
                .StartNow()
                .WithIdentity(typeof(DailyRateDownloadingJob).GetNameAttribute(), _name)
                .WithCronSchedule(SchedulerSettings.DailyRateDownloadingCronExpression, x => x.InTimeZone(TimeZoneInfo.Utc))
                .ForJob(nameof(DailyRateDownloadingJob), _name)
                .Build();

            var historyRateDownloadingTrigger = TriggerBuilder.Create()
                .StartAt(DateBuilder.FutureDate(2, IntervalUnit.Minute))
                .WithIdentity(typeof(HistoryRateDownloadingJob).GetNameAttribute(), _name)
                .ForJob(nameof(HistoryRateDownloadingJob), _name)
                .Build();

            await _scheduler.ScheduleJob(dailyRateDownloadingJob, dailyRateDownloadingTrigger);
            await _scheduler.ScheduleJob(historyRateDownloadingJob, historyRateDownloadingTrigger);
        }
    }
}
