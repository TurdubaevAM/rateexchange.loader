﻿namespace RateExchange.Service.DataTypes {
    public class CurrencyDTO : BaseDTO {
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
