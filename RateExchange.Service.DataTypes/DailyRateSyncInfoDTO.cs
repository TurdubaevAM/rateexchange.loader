﻿using System;

namespace RateExchange.Service.DataTypes {
    public class DailyRateSyncInfoDTO : BaseDTO {
        public DateTime LastSyncDate { get; set; }
    }
}
