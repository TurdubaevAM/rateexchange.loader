﻿using System.Collections.Generic;

namespace RateExchange.Service.DataTypes {
    public sealed class MonthRateExchangeReportDTO {
        public int Year { get; set; }
        public string Month { get; set; }

        public List<WeekRateExchangeReportDTO> WeekPeriods { get; set; }
    }
}
