﻿using RateExchange.Common.Enum;
using System.Threading.Tasks;

namespace RateExchange.Service.DataTypes.Export {
    public interface IExported {
        Task<byte[]> ExportAsync(FileType type);
    }
}
