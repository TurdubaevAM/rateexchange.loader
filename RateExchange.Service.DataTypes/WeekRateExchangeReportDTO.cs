﻿using System.Collections.Generic;

namespace RateExchange.Service.DataTypes {
    public sealed class WeekRateExchangeReportDTO {
        public int WeekStartDayNumber { get; set; }
        public int WeekEndDayNumber { get; set; }
        public List<RateExchangeInfoDTO> RatesExchange { get; set; }
    }
}
