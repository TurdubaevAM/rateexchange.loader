﻿using RateExchange.Common.Utility.Parser;

namespace RateExchange.Service.DataTypes.Parser {
    public class DailyRateExchangeDocumentMetaData : IMetaData {
        public string Description { get; set; }
    }
}
