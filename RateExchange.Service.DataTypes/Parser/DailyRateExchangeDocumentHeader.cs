﻿using RateExchange.Common.Utility.Parser;
using System.Collections.Generic;

namespace RateExchange.Service.DataTypes.Parser {
    public class DailyRateExchangeDocumentHeader : IHeader {
        public IReadOnlyList<string> Columns { get; set; }
    }
}
