﻿using RateExchange.Common.Utility.Parser;
using System.Collections.Generic;

namespace RateExchange.Service.DataTypes.Parser {
    public class DailyRateExchangeDocument : IDocument<DailyRateExchangeDocumentMetaData, DailyRateExchangeDocumentHeader, DailyRateExchangeDocumentRow> {
        public DailyRateExchangeDocumentMetaData MetaData { get; set; }
        public IReadOnlyList<DailyRateExchangeDocumentRow> Records { get; set; }
        public DailyRateExchangeDocumentHeader Header { get; set; }
    }
}
