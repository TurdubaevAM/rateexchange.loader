﻿namespace RateExchange.Service.DataTypes {
    public class CountryDTO : BaseDTO {
        public string Name { get; set; }
    }
}
