﻿namespace RateExchange.Service.DataTypes {
    public sealed class RateExchangeInfoDTO {
        public string Currency { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public decimal Mediana { get; set; }
    }
}
