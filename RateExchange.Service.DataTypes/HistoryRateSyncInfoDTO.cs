﻿using System;

namespace RateExchange.Service.DataTypes {
    public class HistoryRateSyncInfoDTO : BaseDTO {
        public int SyncYear { get; set; }
        public DateTime LastSyncDate { get; set; }
    }
}
