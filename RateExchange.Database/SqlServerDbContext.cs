﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Common.Settings;

namespace RateExchange.Database {
    public class SqlServerDbContext : DbContextBase {
        public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options)
               : base(options) {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            if (!optionsBuilder.IsConfigured) {
                optionsBuilder.UseSqlServer(AppSettings.ConnectionString);
            }

            base.OnConfiguring(optionsBuilder);
        }

    }
}
