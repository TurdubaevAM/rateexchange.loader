﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RateExchange.Database.Entity {
    [Table("DailyRateSyncInfo")]
    public class DailyRateSyncInfo : EntityBase {
        public DateTime LastSyncDate { get; set; }
    }
}
