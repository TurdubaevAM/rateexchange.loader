﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RateExchange.Database.Entity {
    [Table("Country")]
    public class Country : EntityBase {
        public string Name { get; set; }

        public virtual ICollection<Currency> Currency { get; set; }
        public virtual ICollection<DailyRateExchange> DailyRate { get; set; }
    }
}
