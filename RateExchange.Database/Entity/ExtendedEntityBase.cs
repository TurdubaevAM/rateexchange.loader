﻿using System;

namespace RateExchange.Database.Entity {
    internal class ExtendedEntityBase : EntityBase {
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
