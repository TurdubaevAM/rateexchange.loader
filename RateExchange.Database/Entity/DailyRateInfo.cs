﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RateExchange.Database.Entity {
    [Table("DailyRateInfo")]
    public class DailyRateInfo : EntityBase {
        public DateTime Date { get; set; }
        public int Number { get; set; }

        public virtual ICollection<DailyRateExchange> Rates { get; set; }
    }
}
