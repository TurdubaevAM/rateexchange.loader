﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RateExchange.Database.Entity {
    [Table("Currency")]
    public class Currency : EntityBase {
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<DailyRateExchange> DailyRate { get; set; }
    }
}
