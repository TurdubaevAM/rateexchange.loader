﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RateExchange.Database.Entity {
    [Table("DailyRateExchange")]
    public class DailyRateExchange : EntityBase {
        public int Amount { get; set; }
        public decimal Rate { get; set; }

        public int CountryId { get; set; }
        public int CurrencyId { get; set; }
        public int DailyRateInfoId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual DailyRateInfo DailyRate { get; set; }

    }
}
