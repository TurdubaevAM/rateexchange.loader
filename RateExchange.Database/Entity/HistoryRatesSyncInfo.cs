﻿using System;

namespace RateExchange.Database.Entity {
    public class HistoryRatesSyncInfo : EntityBase {
        public int SyncYear { get; set; }
        public DateTime LastSyncDate { get; set; }
    }
}
