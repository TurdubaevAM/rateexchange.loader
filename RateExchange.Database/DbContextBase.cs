﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Database.Entity;

namespace RateExchange.Database {
    public abstract class DbContextBase : DbContext {

        public DbContextBase(DbContextOptions options) : base(options) {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<DailyRateInfo> DailyRateInfo { get; set; }
        public DbSet<DailyRateExchange> DailyRateExchange { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<HistoryRatesSyncInfo> HistoryRatesSyncInfo { get; set; }
        public DbSet<DailyRateSyncInfo> DailyRateSyncInfo { get; set; }
    }
}
