﻿using System.Collections.Generic;

namespace RateExchange.Common.Utility.Parser {
    public interface IHeader {
        IReadOnlyList<string> Columns { get; set; }
    }
}
