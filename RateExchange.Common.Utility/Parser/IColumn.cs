﻿
namespace RateExchange.Common.Utility.Parser {

    public interface IColumn<TValue> {
        TValue Value { get; set; }
    }
}
