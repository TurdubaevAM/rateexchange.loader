﻿namespace RateExchange.Common.Utility.Parser {
    public interface IMetaData {
        string Description { get; set; }
    }
}
