﻿using System.Collections.Generic;

namespace RateExchange.Common.Utility.Parser {

    public interface IDocument<TMetaData, THeader, TRow> : IDocument<THeader, TRow> 
        where TRow : IRow{
        TMetaData MetaData { get; set; }
    }

    public interface IDocument<THeader, TRow> {
        IReadOnlyList<TRow> Records { get; set; }
        THeader Header { get; set; }
    }
}
