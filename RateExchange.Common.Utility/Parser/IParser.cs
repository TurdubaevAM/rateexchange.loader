﻿using System.Threading.Tasks;

namespace RateExchange.Common.Utility.Parser {
    public interface IParser<TDocument, TMetaData, THeader, TRow> 
        where TDocument : IDocument<TMetaData, THeader, TRow>
        where TRow : IRow {

        Task<TDocument> LoadAsync(string filePath);
    }

    public interface IParser<TDocument, THeader, TRow>
        where TDocument : IDocument<THeader, TRow> {

        Task<TDocument> LoadAsync(string filePath);
    }
}
