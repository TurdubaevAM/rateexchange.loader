﻿using RateExchange.Common.Utility.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Common.Utility.Parser {
    public sealed class TextFileWithMetadataParser<TDocument, TMetaData, THeader, TRow> : IParser<TDocument, TMetaData, THeader, TRow>
        where TDocument : IDocument<TMetaData, THeader, TRow>, new()
        where TMetaData : IMetaData, new()
        where THeader : IHeader, new()
        where TRow : IRow, new() {

        private readonly ParserHelper _parser;

        public TextFileWithMetadataParser() {
            _parser = new ParserHelper();
        }

        public async Task<TDocument> LoadAsync(string filePath) {
            if (String.IsNullOrWhiteSpace(filePath))
                throw new ArgumentNullException(nameof(filePath));

            if (!File.Exists(filePath))
                throw new FileNotFoundException("The parsed file was not found on the given path. ", filePath);

            int lineNumber = 1;
            string currentLine;

            using (var reader = File.OpenText(filePath)) {
                var metaData = new TMetaData();
                var header = new THeader();
                var records = new List<TRow>();

                var separator = new char[] { '|' };
                const int metaDataLineNumber = 1;
                const int headerLineNumber = 2;

                while ((currentLine = await reader.ReadLineAsync()) != null) {
                    if (lineNumber == metaDataLineNumber) {
                        metaData.Description = currentLine;
                        lineNumber++;
                        continue;
                    }

                    var parsedResult = (await _parser.ParseLineAsync(currentLine, separator)).ToList();
                    if (lineNumber == headerLineNumber) {
                        header.Columns = parsedResult;
                        lineNumber++;
                        continue;
                    }

                    // TODO: add Data Type Mapping here in future
                    var record = new TRow();
                    record.Columns = (await _parser.ParseLineAsync(currentLine, separator)).ToList();
                    records.Add(record);

                    lineNumber++;
                }

                var document = new TDocument() {
                    Header = header,
                    MetaData = metaData,
                    Records = records
                };
                return document;
            }
        }
    }
}
