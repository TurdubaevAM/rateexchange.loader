﻿using System.Collections.Generic;

namespace RateExchange.Common.Utility.Parser {
    public interface IRow {
        IReadOnlyList<string> Columns { get; set; }
    }
}
