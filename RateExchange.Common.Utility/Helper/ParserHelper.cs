﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RateExchange.Common.Utility.Helper {
    public sealed class ParserHelper {
        public async Task<IEnumerable<string>> ParseLineAsync(string text, char[] separator) {
            if (String.IsNullOrWhiteSpace(text))
                return await Task.FromResult(new List<string>());

            var parsedArray = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return parsedArray;
        }

        public async Task<IEnumerable<string>> ParseLineAsync(string text, string[] separator) {
            if (String.IsNullOrWhiteSpace(text))
                return await Task.FromResult(new List<string>());

            var parsedArray = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return parsedArray;
        }
    }
}
