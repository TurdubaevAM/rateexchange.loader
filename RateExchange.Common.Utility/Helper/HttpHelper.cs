﻿using RateExchange.Common.Exceptions;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace RateExchange.Common.Utility.Helper {
    public static class HttpHelper {
        public static async Task DownloadFileAsync(string url, string destination) {
            try {
                if (String.IsNullOrEmpty(url))
                    throw new ArgumentNullException(nameof(url));

                if (String.IsNullOrEmpty(destination))
                    throw new ArgumentNullException(nameof(destination));

                Directory.CreateDirectory(Path.GetDirectoryName(destination));
                
                using (var client = new WebClient()) {
                    await client.DownloadFileTaskAsync(new Uri(url), destination);
                }
            }
            catch (Exception ex) {
                throw new FileDownloadingException(ex.Message, ex);
            }
        }
    }
}
