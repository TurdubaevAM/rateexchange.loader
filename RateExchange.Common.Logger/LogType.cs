﻿namespace RateExchange.Common.Logger {
    public enum LogType {
        INFO = 1,
        WARNING = 2,
        ERROR = 3,
        DEBUG = 4
    }
}
