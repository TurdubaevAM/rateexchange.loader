﻿using NLog;

namespace RateExchange.Common.Logger.Extension {
    public static class LogExtension {
        public static LogLevel ConvertToLevel(this LogType type) {
            LogLevel level;
            switch (type) {
                case LogType.INFO:
                    level = LogLevel.Info;
                    break;

                case LogType.WARNING:
                    level = LogLevel.Warn;
                    break;

                case LogType.DEBUG:
                    level = LogLevel.Debug;
                    break;

                case LogType.ERROR:
                    level = LogLevel.Error;
                    break;

                default:
                    level = LogLevel.Info;
                    break;
            }

            return level;
        }
    }
}
