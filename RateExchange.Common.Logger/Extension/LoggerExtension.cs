﻿using System;
using System.Threading.Tasks;

namespace RateExchange.Common.Logger.Extension {
    public static class LoggerExtension {
        public static async Task LogAsync(this Exception ex, LogType type = LogType.ERROR) {
            var logger = FileLogger.GetInstance();
            await logger.LogAsync(ex, type);
        }

        public static async Task LogAsync(this string message, LogType type = LogType.INFO) {
            var logger = FileLogger.GetInstance();
            await logger.LogAsync(message, type);
        }
    }
}
