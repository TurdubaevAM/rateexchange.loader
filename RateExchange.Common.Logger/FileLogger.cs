﻿using NLog;
using RateExchange.Common.Logger.Extension;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Timers;

namespace RateExchange.Common.Logger {
    public class FileLogger : IAppLogger {
        private readonly ConcurrentQueue<Log> _messageQueue;
        private readonly NLog.Logger _logger;
        private static Timer _timer;

        private static Lazy<IAppLogger> _instance;

        private FileLogger() {
            _messageQueue = new ConcurrentQueue<Log>();
            _logger = LogManager.GetCurrentClassLogger();

            _timer = new Timer(30000);
            _timer.Elapsed += OnTimeElapsed; ;
            _timer.AutoReset = true;
            _timer.Enabled = true;
            _timer.Start();
        }

        private void OnTimeElapsed(object sender, ElapsedEventArgs e) {
            if (!_messageQueue.IsEmpty) {
                WriteToFile();
            }
        }

        private void WriteToFile() {
            lock (_logger) {
                while (_messageQueue.TryDequeue(out Log log)) {
                    var logEvent = new LogEventInfo() {
                        Level = log.LogType.ConvertToLevel(),
                        Message = String.IsNullOrWhiteSpace(log.StackTrace)
                            ? String.Format("{0}: \t {1} \n\t ", log.Date.ToString("yyyyMMddHHmmss"), log.Message)
                            : String.Format("{0}: \t {1} \n\t {2}", log.Date.ToString("yyyyMMddHHmmss"), log.Message, log.StackTrace),
                    };

                    _logger.Log(logEvent);
                }
            }
        }


        public static IAppLogger GetInstance() {
            if (_instance == null) {
                _instance = new Lazy<IAppLogger>(() => new FileLogger(), isThreadSafe: true);
            }

            return _instance.Value;
        }

        public async Task LogAsync(string message, LogType type = LogType.INFO) {
            if (String.IsNullOrWhiteSpace(message))
                return;

            var log = new Log() {
                Date = DateTime.UtcNow,
                LogType = type,
                Message = message,
            };
            _messageQueue.Enqueue(log);
        }

        public async Task LogAsync(Exception ex, LogType type = LogType.ERROR) {
            if (ex == null)
                return;

            var log = new Log() {
                Date = DateTime.UtcNow,
                LogType = type,
                Message = ex.Message,
                StackTrace = ex.StackTrace
            };

            _messageQueue.Enqueue(log);
        }

        public void ForceWrite() {
            WriteToFile();
        }
    }
}
