﻿using System;
using System.Threading.Tasks;

namespace RateExchange.Common.Logger {
    public interface IAppLogger {
        Task LogAsync(string message, LogType type = LogType.INFO);
        Task LogAsync(Exception ex, LogType type = LogType.ERROR);
        void ForceWrite();
    }
}
