﻿using System;

namespace RateExchange.Common.Logger {
    public class Log {
        public LogType LogType { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
