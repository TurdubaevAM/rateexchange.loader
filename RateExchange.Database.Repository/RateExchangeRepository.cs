﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Database.Entity;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Service.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Database.Repository {
    public class RateExchangeRepository : BaseRepository<DailyRateInfo>, IRateExchangeRepository {
        public RateExchangeRepository(DbContextBase dbContext)
            : base(dbContext) {
        }

        public async Task AddDailyRateExchangeAsync(DailyRateInfo dailyRateInfo) {
            await DbContext.DailyRateInfo.AddAsync(dailyRateInfo);
            await DbContext.DailyRateExchange.AddRangeAsync(dailyRateInfo.Rates.ToList());
            await DbContext.SaveChangesAsync();
        }

        public async Task AddDailyRateExchangeAsync(List<DailyRateInfo> dailyRatesInfo) {
            await DbContext.DailyRateInfo.AddRangeAsync(dailyRatesInfo.Select(x => x).ToList());
            await DbContext.DailyRateExchange.AddRangeAsync(dailyRatesInfo.SelectMany(x => x.Rates).ToList());
            await DbContext.SaveChangesAsync();
        }

        public async Task<MonthRateExchangeReportDTO> GetRateExchangeReport(DateTime beginDate, DateTime endDate, IEnumerable<int> currencies) {

            const int daysPerWeek = 7;
            string month = beginDate.ToString("MMMM");

            var qury = await DbContext.DailyRateExchange
                .Include(x => x.DailyRate)
                .Include(x => x.Currency)
                .Where(x => x.DailyRate.Date >= beginDate && x.DailyRate.Date < endDate && currencies.Contains(x.CurrencyId))
                .GroupBy(x => x.DailyRate.Date.DayOfYear / daysPerWeek)
                .ToListAsync();

            var result = qury.Select(x => new WeekRateExchangeReportDTO() {
                WeekStartDayNumber = x.Min(i => i.DailyRate.Date.Day),
                WeekEndDayNumber = x.Max(i => i.DailyRate.Date.Day),
                RatesExchange = x
                        .GroupBy(rate => new { CurrencyId = rate.CurrencyId, CurrencyAmount = rate.Amount })
                        .Select(rate => new RateExchangeInfoDTO() {
                            Max = rate.Max(r => r.Rate) / rate.Key.CurrencyAmount,
                            Min = rate.Min(r => r.Rate) / rate.Key.CurrencyAmount,
                            Mediana = rate.Average(r => r.Rate) / rate.Key.CurrencyAmount,
                            Currency = rate.FirstOrDefault().Currency.Code
                        }).ToList()
            }).ToList();

            var report = new MonthRateExchangeReportDTO() {
                Month = month,
                Year = beginDate.Year,
                WeekPeriods = result
            };

            return report;
        }
    }
}
