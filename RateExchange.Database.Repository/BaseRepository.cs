﻿using Microsoft.EntityFrameworkCore;
using RateExchange.Database.Repository.Abstraction;
using RateExchange.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateExchange.Database.Repository {
    public class BaseRepository<T, TK> : IRepository<T, TK>
            where T : class
            where TK : struct {

        protected readonly DbSet<T> Set;
        protected readonly DbContextBase DbContext;

        public BaseRepository(DbContextBase dbContext) {
            if (dbContext == null)
                throw new ArgumentNullException(nameof(dbContext));

            Set = dbContext.Set<T>();
            DbContext = dbContext;
        }

        public T GetById(TK id) {
            return Set.Find(id);
        }

        public async Task<T> GetByIdAsync(TK id) {
            return await Set.FindAsync(id);
        }

        public T Create(T newEntity) {
            return Set.Add(newEntity).Entity;
        }

        public void Update(T entity) {
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual T Delete(TK id) {
            var entity = Set.Find(id);

            if (entity == null)
                throw new KeyNotFoundException();

            return Set.Remove(entity).Entity;
        }

        public void DeleteMany(List<TK> ids) {
            foreach (var id in ids) {
                Delete(id);
            }
        }

        public IEnumerable<T> GetAll() {
            return DbContext.Set<T>();
        }

        public void Commit() {
            DbContext.SaveChanges();
        }

        public async Task<int> CommitAsync() {
            return await DbContext.SaveChangesAsync();
        }

        public IQueryable<T> AsQueryable() {
            return Set.AsQueryable();
        }

        public void Dispose() {
            DbContext?.Dispose();
        }
    }

    public class BaseRepository<T> : BaseRepository<T, int>, IRepository<T>
        where T : class {

        public BaseRepository(DbContextBase dbContext)
            : base(dbContext) {
        }
    }
}
